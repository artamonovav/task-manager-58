package ru.t1.artamonov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.dto.model.SessionDTO;
import ru.t1.artamonov.tm.dto.model.UserDTO;

public interface IAuthService {

    @NotNull
    String login(@Nullable String login, @Nullable String password);

    @NotNull
    SessionDTO validateToken(@Nullable String token);

    @NotNull
    UserDTO registry(@Nullable String login, @Nullable String password, @Nullable String email);

    void logout(@Nullable SessionDTO session);

}
