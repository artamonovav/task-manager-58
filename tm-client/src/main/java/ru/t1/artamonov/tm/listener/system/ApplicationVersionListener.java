package ru.t1.artamonov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.artamonov.tm.dto.request.ServerVersionRequest;
import ru.t1.artamonov.tm.dto.response.ServerVersionResponse;
import ru.t1.artamonov.tm.event.ConsoleEvent;

@Component
public final class ApplicationVersionListener extends AbstractSystemListener {

    @NotNull
    private static final String NAME = "version";

    @NotNull
    private static final String ARGUMENT = "-v";

    @NotNull
    private static final String DESCRIPTION = "Display program version.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@applicationVersionListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[VERSION]");
        System.out.println("[Client]");
        System.out.println(propertyService.getApplicationVersion());
        System.out.println("[Server]");
        @NotNull final ServerVersionRequest request = new ServerVersionRequest(getToken());
        @NotNull final ServerVersionResponse response = serviceLocator.getSystemEndpoint().getVersion(request);
        System.out.println("Client: " + response.getVersion());
    }

}
