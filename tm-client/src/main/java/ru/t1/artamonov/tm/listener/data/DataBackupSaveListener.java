package ru.t1.artamonov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.artamonov.tm.dto.request.DataBackupSaveRequest;
import ru.t1.artamonov.tm.event.ConsoleEvent;

@Component
public final class DataBackupSaveListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "backup-save";

    @NotNull
    private static final String DESCRIPTION = "Save backup to file";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataBackupSaveListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull DataBackupSaveRequest request = new DataBackupSaveRequest(getToken());
        domainEndpointClient.saveDataBackup(request);
    }

}
